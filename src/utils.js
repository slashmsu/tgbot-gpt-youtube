import { unlink } from 'fs/promises'

const userUsages = {}
const userSession = {} // { userId: {messages: messages} }

const MAX_TOKENS = 14_000

export async function removeFile(path) {
  try {
    await unlink(path)
  } catch (e) {
    console.log('Error while removing file', e.message)
  }
}

export function isCommandInProcess(ctx) {
  const userId = ctx.from.id
  return userUsages[userId] !== undefined
}

export async function addCommandInProcess(ctx) {
  const userId = ctx.from.id
  userUsages[userId] = true
}

export async function removeCommandInProcess(ctx) {
  const userId = ctx.from.id
  delete userUsages[userId]
}


export function getUserMessages(ctx) {
  const userId = ctx.from.id
  return userSession[userId].messages
}

export function resetUserMessages(ctx) {
  const userId = ctx.from.id
  if (!!userId) {
    userSession[userId].messages = []
  }
}

export function addUserMessage(ctx, message) {
  const userId = ctx.from.id
  if (!userSession[userId]) {
    userSession[userId] = {
      messages: [],
    }
  }
  userSession[userId].messages.push(message)
}

// OpenAI model 3.5 turbo may work only with 4000 tokens
export function normalizeMessages(ctx, response) {

  if (response.data.usage.total_tokens > MAX_TOKENS) {
    const messages = getUserMessages(ctx)
    messages.shift()
    messages.shift()
  }

}

export function startTypingAnimation(ctx) {
  ctx.telegram.sendChatAction(ctx.chat.id, 'typing'); // Show the bot "typing" while processing the request
  return setInterval(() => {
    ctx.telegram.sendChatAction(ctx.chat.id, 'typing'); // Extend typing animation
  }, 5000); // Extend the typing animation every 5 seconds
}
