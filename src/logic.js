import {openai} from './openai.js'
import {addUserMessage, resetUserMessages, startTypingAnimation} from "./utils.js";

const resetWords = [
    'сброс',
    'сбросить',
    'сбрось',
    'сначала',
    'все с начала',
    'начать сначала',
    'начать заново',
    'заново',
    'сброс контекста',
    'сбросить контекст',
    'начать с начала',
    'в начало',
    'сбросить диалог',
    'начать диалог заново',
    'сброс диалога',
    'начать с чистого листа',
    'сбросить все',
    'очистить контекст',
    'забыть все',
    'начать все заново',
    'все заново',
    'всё заново',
    'сбросить настройки',
    'сброс настроек',
    'сбросить параметры',
    'сброс параметров',
    'в начало разговора',
    'начать разговор заново',
    'начать диалог заново',
    'начать заново диалог',
    'начать диалог с начала',
    'сбросить историю',
    'очистить историю',
    'начать все сначала',
    'начать чистый лист'
];

export const INITIAL_SESSION = {
    messages: [],
}

export async function initCommand(ctx) {
    ctx.session = INITIAL_SESSION
    resetUserMessages(ctx)
    await ctx.reply('Жду вашего голосового или текстового сообщения')
}

export async function processTextToChat(ctx, content) {
    let intervalId = startTypingAnimation(ctx); // Start the typing animation
    try {
        // User message
        const userMessage = {role: openai.roles.USER, content}
        ctx.session.messages.push(userMessage)
        addUserMessage(ctx, userMessage)

        // Response from gpt
        const response = await openai.chat(ctx)

        // Bot message
        const botMessage = {
            role: openai.roles.ASSISTANT,
            content: response.content,
        }
        ctx.session.messages.push(botMessage)
        addUserMessage(ctx, botMessage)

        clearInterval(intervalId); // Stop the typing animation
        await ctx.reply(response.content)
    } catch (e) {
        clearInterval(intervalId); // Stop the typing animation
        await ctx.reply('Error while proccesing text to gpt ' + e.message)
        console.log('Error while proccesing text to gpt', e.message)
    }
}

export async function isResetCommand(text) {
    const textWithoutPunctuation = text.replace(/[.!]/g, '');
    return resetWords.includes(textWithoutPunctuation.toLowerCase());
}
