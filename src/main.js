import {Telegraf, session} from 'telegraf'
import {message} from 'telegraf/filters'
import {code} from 'telegraf/format'
import config from 'config'
import {ogg} from './ogg.js'
import {openai} from './openai.js'
import {addCommandInProcess, isCommandInProcess, removeCommandInProcess, removeFile, startTypingAnimation} from './utils.js'
import {initCommand, processTextToChat, INITIAL_SESSION, isResetCommand} from './logic.js'

const bot = new Telegraf(config.get('TELEGRAM_TOKEN'))

bot.use(session())

bot.command('new', initCommand)

bot.command('start', initCommand)

bot.on(message('voice'), async (ctx) => {
    ctx.session ??= INITIAL_SESSION
    try {
        if (isCommandInProcess(ctx) === true) {
            ctx.reply('Команда уже выполняется')
            return;
        }

        await addCommandInProcess(ctx)

        let intervalId = startTypingAnimation(ctx); // Start the typing animation
        // await ctx.reply(code('Сообщение принял. Жду ответ от сервера...'))
        const link = await ctx.telegram.getFileLink(ctx.message.voice.file_id)
        const userId = String(ctx.message.from.id)
        const oggPath = await ogg.create(link.href, userId)
        // console.log("oggPath", oggPath)
        const mp3Path = await ogg.toMp3(oggPath, userId)
        // console.log("mp3Path", mp3Path)

        await removeFile(oggPath)

        const text = await openai.transcription(mp3Path)
        if (await isResetCommand(text)) {
            console.log('Сброс контекста')
            clearInterval(intervalId); // Stop the typing animation
            await ctx.reply(code(`Контекст сброшен`))
            await initCommand(ctx)
        } else {
            console.log('Отправка сообщения в чат')
            clearInterval(intervalId); // Stop the typing animation
            await ctx.reply(code(`Ваш запрос: ${text}`))
            await processTextToChat(ctx, text)
        }

        await removeCommandInProcess(ctx)
    } catch (e) {
        console.log(`Error while voice message`, e)
    }
})

bot.on(message('text'), async (ctx) => {
    ctx.session ??= INITIAL_SESSION

    try {
        // We need to check reset command before command in process
        if (await isResetCommand(ctx.message.text)) {
            console.log('Сброс контекста')
            await ctx.reply(code(`Контекст сброшен`))
            await initCommand(ctx)
            return;
        }
        // We need to check command in process before reset command
        if (isCommandInProcess(ctx) === true) {
            ctx.reply('Команда уже выполняется')
            return;
        }
        await addCommandInProcess(ctx)
        // await ctx.reply(code('Сообщение принял. Жду ответ от сервера...'))
        await processTextToChat(ctx, ctx.message.text)
        await removeCommandInProcess(ctx)
    } catch (e) {
        console.log(`Error while voice message`, e.message)
    }
})

bot.launch()

process.once('SIGINT', () => bot.stop('SIGINT'))
process.once('SIGTERM', () => bot.stop('SIGTERM'))


